import React, { useEffect } from "react";
// import './style.scss'

function Index() {

  return (
    <div className="flex justify-between items-center w-full bg-blue-700  py-4 px-4 md:py-4 md:px-48   ">
      <p className="text-white text-center inline font-bold  ml-0 md:ml-0">
        {" "}
        wknd@2020
      </p>

      <button
        className=" px-10 py-1 text-sm rounded rounded-full shadow border  text-white-500 float-right text-white mr-0 md:px-24 mr-2 "
        onClick={() => alert("Not Implement!")}
      >
        alpha version 0.1
      </button>
    </div>
  );
}

export default Index;
