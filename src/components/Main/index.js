import React from "react";
import "./style.scss";
import img1 from "../../assets/img/Path2.png";
import img2 from "../../assets/img/Path1.png";
import Hero from "../../assets/img/Hero.png";
function index() {
  return (
    <div className="background-pink ">
      <div className="">
        <img src={img1} className="absolute background-pink pb-52" />
        <img src={img2} className="absolute center-element "  />
      </div>

      <div className="relative">
        <h1
          className="text-center pt-36"
          style={{
            color: "white",
            fontSize: 62,
            fontWeight: 800,
          }}
        >
          {" "}
          WEEKEND FROM HOME
        </h1>
        <p
          className="text-center"
          style={{
            color: "white",
            fontWeight: 800,
            fontStyle: "italic",
          }}
        >
          Stay active with a little workout.
        </p>
        <div className="flex justify-center">
          <img src={Hero} className="center-element pt-8" style={{height:400}} />

          <button className="absolute mt-56 px-24 py-2 text-sm rounded rounded-full shadow bg-slate-100 hover:bg-slate-200 text-slate-500" onClick={()=>alert('Not Implement!')}>
          Let’s Go
          </button>
        </div>
      </div>
    </div>
  );
}

export default index;
