import React, { useEffect, useState } from "react";
import HeroImg from "../../assets/img/Hero.png";
import { useDispatch, useSelector } from "react-redux";
import { setDataHelp } from "../../config/redux/action";
function Index() {
  const { dataHelp, page } = useSelector((state) => state.helpTips);

  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(setDataHelp());
  }, [dispatch]);

  return (
    <div className="bg-black text-white text-center w-full">
      <br />
      <br />
      <br />

      <h1
        style={{ fontSize: 32, fontWeight: 800 }}
        className="items-center justify-center"
      >
        {" "}
        POV
      </h1>
      <div className="flex items-center justify-center">
        <div
          className=" font-bold rounded-lg pl-10 pr-10 pt-5 inline  w-w-5/6 md:w-3/6  "
          //   style={{ marginTop: -200 }}
        >
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
          minim veniam, quis nostrud ullamco laboris nisi ea commodo consequat.
          Duis aute irure dolor in reprehenderit in voluptate velit esse cillum
          dolore eu fugiat nulla pariatur.
        </div>
      </div>

      <h1
        style={{ fontSize: 32, fontWeight: 800 }}
        className="items-center justify-center mt-20"
      >
        {" "}
        Resource
      </h1>
      <div className="flex items-center justify-center">
        <div
          className=" font-bold rounded-lg pl-10 pr-10 pt-5 inline  w-w-5/6 md:w-3/6  "
          //   style={{ marginTop: -200 }}
        >
          These cases are perfectly simple and easy to distinguish. In a free
          hour, when our power of choice is untrammelled and when nothing
          prevents our being able to do what we like best.
        </div>
      </div>

      <h1
        style={{ fontSize: 32, fontWeight: 800 }}
        className="items-center justify-center mt-20"
      >
        {" "}
        Help & Tips
      </h1>

      <div className="grid grid-cols-1 md:grid-cols-3 gap-2">
        {dataHelp.map((data) => (
          <div className="container mx-auto px-4" key={data?.id}>
            <div className="flex flex-wrap -mx-1 lg:-mx-4">
              <div className="w-full ">
                <article className="overflow-hidden rounded-lg shadow-lg">
                  <img
                    alt={data?.slug}
                    className="block h-auto w-full"
                    src={data?.image}
                    //  key={data?.id}
                  />
                  <header
                    className="flex items-center leading-tight p-2 md:p-4  w-full backdrop-blur-sm"
                    style={{ marginTop: -38 }}
                    //  key={data?.id}
                  >
                    <h1
                      className="text-center text-white ml-auto mr-auto"
                      style={{ marginTop: -10 }}
                    >
                      {data?.title}
                    </h1>
                  </header>
                </article>
              </div>
            </div>
          </div>
        ))}
      </div>

      <h1
        style={{ fontSize: 32, fontWeight: 800 }}
        className="items-center justify-center mt-20"
      >
        {" "}
        You’re all set.
      </h1>
      <div className="float-left bg-black text-white w-full">
        <img
          src={HeroImg}
          className="float-left rotate-180 "
          width={80}
          //   style={{ width: 140, marginRight: -70, backgroundColor: "" }}
        />
        <div className="flex items-center justify-center">
          <div
            className="rounded-lg pl-10 pr-10 inline  text-center w-w-5/6 md:w-3/6  " style={{marginTop:-100}}
            //   style={{ marginTop: -200 }}
          >
            The wise man therefore always holds in these matters to this
            principle of selection.
          </div>
          <br/>
          <br/>
          <br/>
          <br/>
          <br/>
          <br/>
          <br/>
          <br/>
        </div>
      </div>
    </div>
  );
}

export default Index;
