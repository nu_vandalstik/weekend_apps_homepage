import axios from "axios";

export const setDataHelp = (page) => (dispatch) => {
  axios
    .get(`https://wknd-take-home-challenge-api.herokuapp.com/help-tips`)
    .then((result) => {
      const responseAPI = result.data;
      console.log(responseAPI)

      dispatch({ type: "UPDATE_DATA_HELP", payload: responseAPI });
      dispatch({
        type: "UPDATE_PAGE",
         payload: {
           currentPage: responseAPI.current_page, 
        totalPage: Math.ceil(responseAPI.total_data / responseAPI.per_page)
      }
    })
    })
    .catch((err) => {
      console.log("error : ", err);
    });
};  
