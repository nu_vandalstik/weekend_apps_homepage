import reducer from "./reducer/reducer";
import thunk from 'redux-thunk';
import { applyMiddleware, createStore } from "redux";


const store = createStore(reducer, applyMiddleware(thunk));

export default store;
