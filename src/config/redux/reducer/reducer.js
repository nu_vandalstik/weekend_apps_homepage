import { combineReducers } from "redux"
import globalReducer from "./globalReducer"
import helpTips from "./homeReducer"
import dataCard from './cardReducer'
const reducer = combineReducers({helpTips, globalReducer,dataCard
})

export default reducer;