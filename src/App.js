import React from "react";
import Header from "./components/Headers";
import MainPages from "./components/Main";
import Hero from "./components/Hero";
import FootPage from "./components/FootPage";
import Footer from "./components/Footer";
import { Provider } from "react-redux";
import { store } from "./config";

function App() {
  return (
    <Provider store={store}>
      <div className="App">
        <Header />
        <MainPages />
        <Hero />
        <FootPage />
        <Footer />
      </div>
    </Provider>
  );
}

export default App;
